package com.aleksa;

/**
 * Maze solving algorithm that uses boolean flags to determine whether the path is correct
 * by assigning everything different than 0 to true on traversing trough the two-dimensional array.
 * <p>
 * This solution works only on quadratic matrices.
 * <p>
 * {@param wasHere[][] } looks at given value which is then mapped as true, as if a person was at the spot,
 * in it's two-dimensional array, and maps it to itself. Much like if that "person" were
 * to have a blank piece of paper in which he/she wrote the positions that he/she visited.
 * This also serves as "BACKTRACK" in case program
 * wanders off the stray path.
 */
class Maze {

    /**
     * void path() is a method that is traversing trough a two-dimensional array,
     * finds != 0 and marks those values as path.
     *
     * This method also serves as indicator whether that the given maze has a solution or not.
     *
     * @param maze   forwarded two-dimensional array to be solved
     * @param length self explanatory
     */
    void path(int[][] maze, int length) {

        /*
         * wasHere[][] can be seen as the same two-dimensional array
         * that checks !=0 values during traversal.
         * We can imagine the same integer two-dimensional array,
         * but with each iteration, one != 0 value becomes marked
         * with value { true } to indicate that our program,
         * " person " if you will, was at that position (coordinate).
         */
        boolean[][] wasHere = new boolean[length][length]; // a boolean two-dimensional array
        boolean flag = false; // boolean flag to mark the checked position for correct path

        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {

                /*
                  Condition that checks for possible path { maze[i][j] != 0 },
                  for exit from the maze { maze[i][j] == 3 },
                  and checks if that path was not visited { !wasHere[i][j] }
                  */
                if (maze[i][j] != 0 && maze[i][j] == 3 && !wasHere[i][j]) {
                    if (isPath(maze, i, j, wasHere)) {
                        flag = true; // once we reached the end, the flag will trigger
                        break; // thus complete and break the program
                    }
                }
            }
        }

        /*
          Flag is used for sole purpose of telling whether the maze is solvable or not.
          Simple condition to check whether the forwarded two-dimensional array
          is solvable or not.
          Solvable maze is the maze that can connect coordinates from number 2 to number 3
          using 1's as path.
         */
        if (!flag) {
            System.out.println("Error, no possible way. ");
        }
    }

    /**
     * Method that prevents @ArrayOutOfBoundsException by allowing it to go to
     * edges of the two-dimensional array, but never over the edge.
     * <p>
     * The condition:
     * { i >= 0 && i < maze.length } start and limitation condition for rows
     * { j >= 0 && j < maze[0].length } start and limitation condition for columns
     *
     * @param i    row of the two-dimensional array
     * @param j    column of the tho-dimensional array
     * @param maze forwarded two-dimensional array as parameter
     * @return true if the coordinates fall under the given condition
     */
    private boolean safePath(int i, int j, int[][] maze) {
        return i >= 0 && i < maze.length && j >= 0 && j < maze[0].length;
    }

    /**
     * Method which functions as checker and mover.
     * Checks the path for free coordinates (coordinates that hold number different than 0).
     * For each step that this method checks and takes, if the condition is met,
     * the boolean two-dimensional array will mark that particular coordinate as true.
     *
     * @param maze    takes two-dimensional array as parameter
     * @param i       row of the array
     * @param j       column of the array
     * @param wasHere boolean two-dimensional array
     * @return false if condition is incorrect
     */
    private boolean isPath(int[][] maze, int i, int j, boolean[][] wasHere) {

        /*
          Condition that checks safePath() method, looks for numbers other than 0
          AND whether this position is visited (example: a person in maze WAS NOT on that position = !wasHere[i][j] )
         */
        if (safePath(i, j, maze) && maze[i][j] != 0 && !wasHere[i][j]) {
            wasHere[i][j] = true;   // if the condition is met, if the coordinate was the place that the program was not on
            // it will be mapped as if our person was there

            if (maze[i][j] == 2) { // starts if it finds number 2
                System.out.print("(" + i + ", " + j + ") ");
                return true;
            }

            boolean checkRight = isPath(maze, i, j + 1, wasHere); // recursive call to check and move right
            if (checkRight) { // if the condition is true
                System.out.print("(" + i + ", " + j + ") ");
                return true;
            }

            boolean checkLeft = isPath(maze, i, j - 1, wasHere); // recursive call to check and move left
            if (checkLeft) { // if the condition is true
                System.out.print("(" + i + ", " + j + ") ");
                return true;
            }

            boolean checkDown = isPath(maze, i + 1, j, wasHere); // recursive call to check and move down
            if (checkDown) { // if the condition is true
                System.out.print("(" + i + ", " + j + ") ");
                return true;
            }

            boolean checkUp = isPath(maze, i - 1, j, wasHere); // recursive call to check and move up
            if (checkUp) { // if the condition is true
                System.out.print("(" + i + ", " + j + ") ");
                return true;
            }
        }

        return false;
    }
}
